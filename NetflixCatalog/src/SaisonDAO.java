import java.util.ArrayList;
import java.util.List;

public class SaisonDAO {
	public List<SaisonDO> getSaisons(){
		ArrayList<SaisonDO> saisons = new ArrayList<SaisonDO>();
		saisons.add(new SaisonDO(1, 1, 1,13));
		saisons.add(new SaisonDO(2, 1, 2, 9));
		saisons.add(new SaisonDO(3, 2, 1, 13));
		saisons.add(new SaisonDO(4, 2, 2, 16));
		saisons.add(new SaisonDO(5, 2, 3, 16));
		saisons.add(new SaisonDO(6, 2, 4, 13));
		saisons.add(new SaisonDO(7, 3, 1, 3));
		saisons.add(new SaisonDO(8, 3, 2, 4));
		saisons.add(new SaisonDO(9, 3, 3, 6));
		saisons.add(new SaisonDO(10, 3, 4, 6));
		return saisons;
	}
}
