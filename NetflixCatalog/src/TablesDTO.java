
public class TablesDTO {
	private String nom_serie;
	private Integer nb_saisons;
	private Integer nb_episodes;
	
	//Getters & setters
	public String getNom_serie() {
		return nom_serie;
	}
	public void setNom_serie(String nom_serie) {
		this.nom_serie = nom_serie;
	}
	public Integer getNb_saisons() {
		return nb_saisons;
	}
	public void setNb_saisons(Integer nb_saisons) {
		this.nb_saisons = nb_saisons;
	}
	public Integer getNb_episodes() {
		return nb_episodes;
	}
	public void setNb_episodes(Integer nb_episodes) {
		this.nb_episodes = nb_episodes;
	}
}
