
public class SaisonDO {
	private Integer id;
	private Integer fk_serie;
	private Integer num_saison;
	private Integer nb_episodes;
	
	SaisonDO (int id, int fk_serie, int num_saison, int nb_episodes){
		this.setId(id);
		this.setFk_serie(fk_serie);
		this.setNum_saison(num_saison);
		this.setNb_episodes(nb_episodes);
	}

	//Getters & setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFk_serie() {
		return fk_serie;
	}

	public void setFk_serie(Integer fk_serie) {
		this.fk_serie = fk_serie;
	}

	public Integer getNum_saison() {
		return num_saison;
	}

	public void setNum_saison(Integer num_saison) {
		this.num_saison = num_saison;
	}

	public Integer getNb_episodes() {
		return nb_episodes;
	}

	public void setNb_episodes(Integer nb_episodes) {
		this.nb_episodes = nb_episodes;
	}
}
