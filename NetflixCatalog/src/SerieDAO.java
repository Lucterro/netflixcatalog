import java.util.ArrayList;
import java.util.List;

public class SerieDAO {
	public List<SerieDO> getSeries(){
		ArrayList<SerieDO> series = new ArrayList<SerieDO>();
		series.add(new SerieDO(1, "La casa de papel", true, 2017));
		series.add(new SerieDO(2, "The 100", false, 2017));
		series.add(new SerieDO(3, "Black Miror", true, 2017));
		return series;
	}
}
