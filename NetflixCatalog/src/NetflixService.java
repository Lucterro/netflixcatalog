import java.util.ArrayList;
import java.util.List;

public class NetflixService {
	private SerieDAO serieDAO;
	private SaisonDAO saisonSAO;
	
	public NetflixService() {
		serieDAO = new SerieDAO();
		saisonSAO = new SaisonDAO();
	}
	
	public List<TablesDTO> getData(){
		final List<SerieDO> series = serieDAO.getSeries();
		final List<SaisonDO> saisons = saisonSAO.getSaisons();
		final List<TablesDTO> listeFinale = new ArrayList<>();	
		for (final SerieDO do_series : series) {
			final TablesDTO dto_tables = map(do_series, saisons);
			listeFinale.add(dto_tables);
		}
		System.out.println("Liste finale:");
		for (int i = 0; i < listeFinale.size(); i++) {
			System.out.print(listeFinale.get(i).getNom_serie() + " " 
		    + listeFinale.get(i).getNb_saisons() + " "
		    + listeFinale.get(i).getNb_episodes() + "\n");
        }
		return listeFinale;
	}
	
	private int countSaisons(int nbSaisons, SerieDO do_series, SaisonDO do_saisons) {
		if(do_series.getId() == do_saisons.getFk_serie()) {
			return nbSaisons+1;
		} else {
			return nbSaisons;
		}
	}
	
	private int countEpisodes(int nbEpisodes, SerieDO do_series, SaisonDO do_saisons) {
		if(do_series.getId() == do_saisons.getFk_serie()) {
			nbEpisodes+= do_saisons.getNb_episodes();
		}
		return nbEpisodes;
	}
	
	private TablesDTO map(final SerieDO do_series, final List<SaisonDO> saisons) {
		final TablesDTO dto_tables = new TablesDTO();
		int nbSaisons = 0;
		int nbEpisodes = 0;
		dto_tables.setNom_serie(do_series.getNom());
		for(final SaisonDO do_saisons : saisons) {
			nbSaisons = countSaisons(nbSaisons, do_series, do_saisons);
			dto_tables.setNb_saisons(nbSaisons);
			nbEpisodes = countEpisodes(nbEpisodes, do_series, do_saisons);
			dto_tables.setNb_episodes(nbEpisodes);
		}
		return dto_tables;
	}
}