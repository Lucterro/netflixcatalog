
public class SerieDO {
	private Integer id;
	private String nom;
	private boolean production_originale;
	private Integer annee;
	
	SerieDO(int id, String nom, boolean prod_ori, int annee){
		this.setId(id);
		this.setNom(nom);
		this.setProduction_originale(prod_ori);
		this.setAnnee(annee);
	}

	//Getters & setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isProduction_originale() {
		return production_originale;
	}

	public void setProduction_originale(boolean production_originale) {
		this.production_originale = production_originale;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}
}
